var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

exports.save = function(data, callback)
{
	var id = -1;

	MongoClient.connect("mongodb://localhost/", function(err, db)
	{
		var myDB = db.db("zombies");
		myDB.createCollection("games", function(err, games)
		{
			games.insert(data, function(err, result) 
			{
				if(!err)
				{
					console.log("Inserted : ");
					console.log(result);
					id = data._id;
					console.log("myid: " + id);
				}
			});
		});
		setTimeout(function(){ db.close();}, 100);
	});

	setTimeout(function(){callback(id);}, 200);

};

exports.load = function(loadid, callback)
{
	var data = null;

	MongoClient.connect("mongodb://localhost/", function(err, db)
	{
		var myDB = db.db("zombies");
		myDB.collection("games", function(err, games)
		{
			games.findOne({_id: new ObjectId(loadid.toString())}, function(err, item)
			{
				data = item;
			});
		});

		setTimeout(function(){ db.close();}, 100);
	});

	setTimeout(function(){
		callback(data);
	}, 200);
}

exports.increment = function(callback)
{
	var count = -1;
    MongoClient.connect("mongodb://localhost/", function(err, db)
    {
        var myDB = db.db("zombies");
        myDB.createCollection("plays", function(err, plays)
        {
            plays.findOne(function(err, item)
            {                                                                                                                                                                                             
                if (!item)                                                                                                                                                                    
                    plays.insert({count:0}, function(err, result)                                                                                                                                         
                    {                                                                                                                                                                                     
                        if (err)                                                                                                                                                                          
                        {                                                                                                                                                                                 
                            console.log(err);                                                                                                                                                             
                        }                                                                                                                                                                                 
                        else                                                                                                                                                                              
						{
							count = 0;
						}
                    });                                                                                                                                                                                   
                else                                                                                                                                                                                      
                {                                                                                                                                                                                         
                    var prevCount = item.count;                                                                                                                                                        
					count = prevCount + 1;
                    plays.update({},{$inc:{count:1}}, function()
					{
						//success
					});
				}
            });
        });
        setTimeout(function(){ db.close();}, 100);
    });

    setTimeout(function(){callback({"count":count});}, 200);

};


exports.plays = function(callback)
{
	var count = -1;
    MongoClient.connect("mongodb://localhost/", function(err, db)
    {
        var myDB = db.db("zombies");
        myDB.createCollection("plays", function(err, plays)
        {
            plays.findOne(function(err, item)
            {                                                                                                                                                                                             
                if (!item)                                                                                                                                                                    
                    plays.insert({count:0}, function(err, result)                                                                                                                                         
                    {                                                                                                                                                                                     
                        if (err)                                                                                                                                                                          
                        {                                                                                                                                                                                 
                            console.log(err);                                                                                                                                                             
                        }                                                                                                                                                                                 
                        else                                                                                                                                                                              
						{
							count = 0;
						}
                    });                                                                                                                                                                                   
                else                                                                                                                                                                                      
                {                                                                                                                                                                                         
					count = item.count;
				}
            });
        });
        setTimeout(function(){ db.close();}, 100);
    });

    setTimeout(function(){callback({"count":count});}, 200);

};
