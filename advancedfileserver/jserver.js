var fs = require('fs');
var url = require('url');
var path = require('path');
var express = require('express');
var app = express();
var mongoAccess = require('./mongoAccess');
var bodyParser = require('body-parser');

if (process.argv.length != 4)
{
    console.log("usage: sudo node advancedfileserver.js port fileOrFolder");
    console.log("you have " + process.argv.length + " arguments");
    return;
}

var port = process.argv[2];
var file = path.resolve(process.argv[3]);

app.listen(port);

app.use(bodyParser.json());

app.post('/save', function(req,res)
{
	var data = req.body;
	mongoAccess.save(data, function(id)
	{
		res.send({"id":id});
	});
});

app.post('/load', function(req,res)
{
	var id = req.body.id;
	mongoAccess.load(id, function(data)
	{
		res.send({"data":data});
	});
});

app.get('/increment', function(req,res)
{
	mongoAccess.increment(function(count)
	{
		res.send(count);
	});
});

app.get('/plays', function(req,res)
{
	mongoAccess.plays(function(count)
	{
		res.send(count);
	});
});

app.get('/*', function(req,res)
{
	/*
    console.log("URL:\t   " + req.originalUrl);
    console.log("Protocol:  " + req.protocol);
    console.log("IP:\t   " + req.ip);
    console.log("Path:\t   " + req.path);
    console.log("Host:\t   " + req.host);
    console.log("Method:\t   " + req.method);
    console.log("Query:\t   " + JSON.stringify(req.query));
    console.log("Fresh:\t   " + req.fresh);
    console.log("Stale:\t   " + req.stale);
    console.log("Secure:\t   " + req.secure);
    console.log("UTF8:\t   " + req.acceptsCharset('utf8'));
    console.log("Connection: " + req.get('connection'));
    console.log("Headers: " + JSON.stringify(req.headers,null,2));
    console.log("");
	*/

    var urlObj = url.parse(req.url, true, false);
    var filename = file + urlObj.pathname;

    fs.exists(filename, function(exists)
    {
        if (!exists)
        {
            res.writeHead(404);
            res.end("<html><h1>FILE NOT FOUND</h1></html>");
            return;
        }

        fs.stat(filename, function(err, stats) {
            if (err)
            {
                console.log(err);
                return;
            }

            if (stats.isDirectory())
            {
                fs.readdir(filename, function(err, list)
                {   
                    if (err)
                    {
                        console.log(err);
                        return;
                    }

                    var response = "<html>";
                    var abs1 = path.resolve(file);
                    var abs2 = path.resolve(filename);
                    var current = abs2.substring(abs1.length);
                    response += "<a href="+current+">.<a><br>";
                    response += "<a href="+current+"/..>..<a><br>";
                    for (var i=0; i<list.length; i++)
                    {   
                        var item = filename;
                        if (item[item.length-1] != '/')
                            item += '/';
                        item += list[i];
                        var abs3 = path.resolve(item);
                        //console.log('abs1: ' + abs1);
                        //console.log('abs2: ' + abs2);

                        // /usr/ec2-usr/public/junk/html
                        var appropriate = abs3.substring(abs1.length);

                        response += "<a href="+appropriate+">"+list[i]+"</a><br>";
                    }
                    response += "</html>";
                    res.end(response);
                    return;
                });
            }
            else
            {
                var strm = fs.createReadStream(filename);
                strm.pipe(res);
            }
        });
    });
});
