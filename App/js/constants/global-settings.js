angular.module("gameApp")
    .constant("globalSettings", {
        spriteSize: 20,
        spriteSheetWidth: 4,
        playerAreaHeight: 6,

        scoreBoardArea: 40,

        scoreMarkerFont: "10px courier",
        scoreBoardFont: "16px courier bold",
        scoreBoardTitleFontColour: "black",
        scoreBoardContentFontColour: "black",
        scoreBoardLivesXPositionText: 50,
        scoreBoardLivesXPositionImage: 45,
        scoreBoardScoreXPosition: 200,
        scoreBoardLevelXPosition: 350,
        scoreBoardHighScoreXPosition: 500,
        scoreBoardTitleYPosition: 15,
        scoreBoardContentYPosition: 35,
        scoreBoardLivesYPosition: 20,
        scoreBoardLivesOffset: 15,
		
		
		elapsedTimeTitleXPosition:   325,
		elapsedTimeTitleYPosition: 15,
		elapsedTimeContentXPosition: 325,
		elapsedTimeContentYPosition: 35,
		elapsedTimeTitle: 'Time',
		elapsedTimeFont: "10px courier",
        elapsedTimeTitleFont: "16px courier bold",
        elapsedTimeTitleFontColour: "black",
        elapsedTimeContentFontColour: "black",
		
        gameOverXPosition: 210,
        gameOverYPosition: 300,
        gameOverFontColour: "black",
        gameOverFont: "32px courier bold",

        gameBoardWidth: 30,
        gameBoardHeight: 30,
        gameBoardBackgroundColour: "white",

        // board creation
        blockChance: 18,

        maxBlocksAllowed: 300,

        delayAfterDeathBeforePlayerRegeneration: 1000,
        delayAfterDeathBeforeNewGameStart: 1500,

        delayAfterDeathBeforeBulletDispose: 2,

        zombieCreationChance: 3,

        maxBulletsOnScreen: 5,
        lives: 1,
		health: 100,
		immunityTime: 25,
    });
