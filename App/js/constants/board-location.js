angular.module("gameApp")
    .constant("boardLocation", {
        space: 0,
        block: 1,
        poisonBlock: 2
    });