angular.module("gameApp")
    .constant("sprite", {
        zombieAnim1Left: 32,
        zombieAnim1Right: 33,
        zombieAnim2Left: 34,
        zombieAnim2Right: 35,
        bullet: 42,
        playerStandStill: 44,
        playerWalkRight1: 45,
        playerWalkRight2: 46,
        playerWalkRight3: 47,
        playerWalkRight4: 48,
        playerFire: 49,
        playerWalkLeft1: 50,
        playerWalkLeft2: 51,
        playerWalkLeft3: 52,
        playerWalkLeft4: 53,
        explosion: 54
    });
