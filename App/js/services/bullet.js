angular.module("gameApp")

    .factory("bulletService", ["globalSettings", "graphicsEngineService", "gameBoardService", "boardLocation", "scoreMarkerService", "zombieService", "characterState", "sprite", "gameStateService", "coordinateSystem",
        function(globalSettings, graphicsEngineService, gameBoardService, boardLocation, scoreMarkerService, zombieService, characterState, sprite, gameStateService, coordinateSystem) {


        var bullets = [];

        var moveBullet = function(bullet) {
            if (bullet.bulletState === characterState.active) {
                //bullet.y--;
		switch(bullet.bulletDirection)
		{
			case 87:
				bullet.y--;
				break;
			case 83:
				bullet.y++;
				break;
			case 65:
				bullet.x--;
				break;
			case 68:
				bullet.x++;
				break;
			default:
				bullet.y--;
				break;
		}
            }

            if (bullet.y < 0 || (bullet.bulletState === characterState.active && checkCollision(bullet.x, bullet.y)) || (bullet.y >= globalSettings.gameBoardHeight)
				|| bullet.x <0 || bullet.x >= globalSettings.gameBoardWidth) {
                bullet.bulletState = characterState.dead;
            }

            if (bullet.bulletState === characterState.dead) {
                bullet.animationDeadCount++;
            }
        };

        var isAlive = function(bullet) {

            return !(bullet.bulletState === characterState.dead && bullet.animationDeadCount > globalSettings.delayAfterDeathBeforeBulletDispose);
        };

        var checkCollision = function(x, y) {

            var didHit = gameBoardService.checkCollision(x, y, true);

			var zombieHit = zombieService.checkCollision(x, y);
            if (zombieHit != -1) {
				didHit = true;
                zombieService.damage(zombieHit);
            }

            return didHit;
        };

        return {
			
			getBullets: function()
			{
				return bullets;
			},
			setBullets: function(b)
			{
				bullets = b;
			},
            createNewBullet: function(x, y) {
                 y++;
				 switch(gameStateService.firingDirection)
			   {
				   case 87:
					y--;
					break;
				case 83:
					y++;
					break;
				case 65:
					x--;
					break;
				case 68:
					x++;
					break;
				default:
					y--;
					break;
			   }
			   var bullet = {
                    x: x,
                    y: y,
                    bulletState: characterState.active,
                    animationDeadCount: 0,
					bulletDirection: gameStateService.firingDirection
                };
				
                if (checkCollision(x, y)) {
                    bullet.bulletState = characterState.dead;
                }

                bullets.push(bullet);
            },

            destroy: function() {
                bullets = [];
            },

            update: function() {
                var aliveBullets = [];

                for (var i = 0; i < bullets.length; i++) {
                    var bullet = bullets[i];

                    moveBullet(bullet);

                    if (isAlive(bullet)) {
                        aliveBullets.push(bullet);
                    }
                }

                bullets = aliveBullets;
            },

            draw: function() {
                for (var i = 0; i < bullets.length; i++) {
                    var bullet = bullets[i];
					graphicsEngineService.drawImageModular(coordinateSystem.world, bullet.x, bullet.y, bullet.bulletState === characterState.active ? 'bullet' : 'explosion');
                }
            }
        }
    }]);
