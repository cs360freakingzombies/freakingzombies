angular.module("gameApp")
    .factory("graphicsEngineService", ["globalSettings", "coordinateSystem", function(globalSettings, coordinateSystem) {
       "use strict";
        return {
            initialise: function(canvasContext, graphicsArray, graphicsDir) {

				console.log('graphicsDir: ' + graphicsDir);
				this.sprites = [];
				for (var i=0; i<graphicsArray.length; i++)
				{
					var sprite = new Object();
					sprite = new Image();
					sprite.src = graphicsDir + graphicsArray[i];
					this.sprites.push(sprite);
				}

                this.spriteWidth = globalSettings.spriteSize;
                this.spriteHeight = globalSettings.spriteSize;
                this.canvas = canvasContext;
                this.spriteSheet = new Image();
                this.spriteSheet.src = graphicsArray[0];
            },
    
            convertGameXCoordinateToPixels: function(x) {
                return x * globalSettings.spriteSize;
            },
    
            convertGameYCoordinateToPixels: function(y) {
                return (y * globalSettings.spriteSize) + globalSettings.scoreBoardArea;
            },
    
            blankScreen: function() {
                this.canvas.fillStyle = globalSettings.gameBoardBackgroundColour;
                this.canvas.fillRect(0, 0, globalSettings.gameBoardWidth * this.spriteWidth, globalSettings.scoreBoardArea + (globalSettings.gameBoardHeight * this.spriteHeight));
            },

            drawText: function(coordSystem, x, y, text, colour, font) {
                if (coordSystem === coordinateSystem.world) {
                    x = this.convertGameXCoordinateToPixels(x);
                    y = this.convertGameYCoordinateToPixels(y);
                }
                this.canvas.fillStyle = colour;
                this.canvas.font = font;
                this.canvas.fillText(text, x, y)
            },

			drawLine: function(x1,y1,x2,y2)
			{
				this.canvas.beginPath();
				this.canvas.moveTo(x1,y1);
				this.canvas.lineTo(x2,y2);
				this.canvas.strokeStyle="black";
				this.canvas.lineWidth=2;
				this.canvas.stroke();
			}, 
    
            drawImage: function(coordSystem, x, y, image) {
                if (coordSystem === coordinateSystem.world) {
                    x = this.convertGameXCoordinateToPixels(x);
                    y = this.convertGameYCoordinateToPixels(y);
                }
                this.canvas.drawImage(
                    this.spriteSheet,
                    this.spriteWidth * (image % globalSettings.spriteSheetWidth),
                    this.spriteHeight * Math.floor(image / globalSettings.spriteSheetWidth),
                    this.spriteWidth,
                    this.spriteHeight,
                    x,
                    y,
                    this.spriteWidth,
                    this.spriteHeight);
            },
			
			drawImageModular: function(coordSystem, x, y, objectName) {
				if (coordSystem === coordinateSystem.world) {
                    x = this.convertGameXCoordinateToPixels(x);
                    y = this.convertGameYCoordinateToPixels(y);
                }
				
				var img;
				for (var i=0; i<this.sprites.length; i++)
				{
					var nm = this.sprites[i].src;
					if (nm.indexOf(objectName) != -1)
					{
						img = this.sprites[i];
						break;
					}
				}

				this.canvas.drawImage(img, x, y, this.spriteWidth, this.spriteHeight);
			}
        }   
    }]);
