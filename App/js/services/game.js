angular.module("gameApp")
    .factory("gameService", ["$http", "graphicsEngineService", "gameBoardService", "globalSettings", "utilsService", "gameStateService", "boardLocation", "characterState", "characterDirection", "sprite", "scoreMarkerService", "zombieService", "bulletService", "playerService", "gameState", "keyPressHandlerService",
        function($http, graphicsEngineService, gameBoardService, globalSettings, utilsService, gameStateService, boardLocation, characterState, characterDirection, sprite, scoreMarkerService, zombieService, bulletService, playerService, gameState, keyPressHandlerService) {
        "use strict";

			var scope = {};

            function resetBoard() {
                zombieService.destroyAll();
                bulletService.destroy();
                gameBoardService.initialise();
				$http.get('/increment').
				success(function(data, status, headers, config)
				{
					console.log("success incrementing: "+JSON.stringify(data));
				}).
				error(function(data, status, headers, config)
				{
					console.log("error: "+data);
				});
			}

            function movePlayer(animation) {
                playerService.update(animation);

                if (animation % 2 === 0 && playerService.isAlive() && playerService.isFiring()) {
                    var playerPosition = playerService.getPreviousPosition();
                    bulletService.createNewBullet(playerPosition.x, playerPosition.y - 1);
                }
            }

            function moveCharacters(animation) {
                bulletService.update(animation);
                zombieService.update(animation);
                movePlayer(animation);
                scoreMarkerService.update(animation);

				//console.log("moving");
                if (animation == 0) {
                    zombieService.checkCreateZombie();
                }

                checkPlayerCollision();
            }

            function checkPlayerCollision() {
                if (!playerService.isAlive()) {
                    return;
                }

                var playerPosition = playerService.getPosition();

                 if (zombieService.checkCollision(playerPosition.x, playerPosition.y) != -1) {

                    if(gameStateService.die())
						playerService.die();
                 }
            }

            function resetAfterPlayerRegeneration() {
                if (gameStateService.isGameOver()) {
                    gameStateService.reset();
                }
                resetBoard();
                playerService.regenerate(true);
                gameStateService.playerRegenerate();
            }

            function levelTransitionUpdate() {
                if (gameStateService.hasLevelTransitionResetAllLines()) {
                    gameStateService.completeLevelTransition();
                }
            }

            function activeGameUpdate(animation) {
				if (scope.paused)
					return false;

				moveCharacters(animation);
				if(gameStateService.immunityTime > 0)
					gameStateService.immunityTime--;
				else if(animation % 2 === 0)
					if(gameStateService.health < 100 && gameStateService.health !== 0)
						gameStateService.health++;
            }

            function playerDeathTransitionUpdate(animation) {
                var keyPress = keyPressHandlerService.getNextMovement();
                if (gameStateService.hasPlayerDeathTransitionComplete() && keyPress.isFiring) {
                    resetAfterPlayerRegeneration();
                } else {
                    moveCharacters(animation);
                }
            }

            function gameOverTransitionUpdate(animation) {
                var keyPress = keyPressHandlerService.getNextMovement();
                if (gameStateService.hasGameOverTransitionComplete() && keyPress.isFiring) {
                    resetAfterPlayerRegeneration();
                } else {
                    moveCharacters(animation);
                }
            }

            return {
                initialise: function(scp) {
					scope = scp;
                    resetBoard();
                    playerService.createPlayer();
                },

                update: function(animation) {
                    switch (gameStateService.gameState) {
                        case gameState.gameActive:
                            activeGameUpdate(animation);
                            break;

                        case gameState.playerDeathTransition:
                            playerDeathTransitionUpdate(animation);
                            break;

                        case gameState.levelTransition:
                            levelTransitionUpdate();
                            break;

                        case gameState.gameOver:
                            gameOverTransitionUpdate(animation);
                            break;
                    }
                }
            }
        }]);
