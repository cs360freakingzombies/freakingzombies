angular.module("gameApp")
    .factory("zombieService", ["globalSettings", "gameBoardService", "graphicsEngineService", "sprite", "utilsService", "playerService", "coordinateSystem",
        function(globalSettings, gameBoardService, graphicsEngineService, sprite, utilsService, playerService, coordinateSystem) {
        "use strict";

		var scope = {};
        var zombies = [];
		var spawned = 0;
		var rate = 1;

		var sign = function(x) {
			return x ? x < 0 ? -1 : 1 : 0;
		}

		var blockFree = function(x,y) {
			if (x>globalSettings.gameBoardWidth-1 || x<0 || y<0 || y>globalSettings.gameBoardHeight-1)
				return false;
			if (!gameBoardService)
				return false;
			if (!gameBoardService.map)
				return false;
			if (!gameBoardService.map[y])
				return false;
			//console.log("map: " + JSON.stringify(gameBoardService.map));
			return gameBoardService.map[y][x] !== 4;
		}

		var zombieFree = function(x,y,oi) {
			for (var i=0; i<zombies.length; i++)
			{
				if (i === oi)
					continue;
				var zombie = zombies[i];
				if (zombie.x === x && zombie.y === y)
					return false;
			}
			return true;
		}

        var create = function(x, y) {
            var zombie = {
                x: x,
                y: y,
                prevX: x,
				prevY: y,
                maxY: globalSettings.gameBoardHeight,
                minY: globalSettings.gameBoardHeight,
				speed: 20,
                sx: 0,
                sy: 0,
				frames: 100,
				stuck: 'none',
				health: 4,
				initX: x,
				initY: y,
				clock: 0
            };

            zombie.startDx = zombie.dx;

			zombies.push(zombie);
        };

        return {

			getZombies: function()
			{
				return zombies;
			},
			setZombies: function(z)
			{
				console.log("setZombies: " + JSON.stringify(z));
				zombies = z;
			},
			initialise: function(scp)
			{
				scope = scp;
			},

            checkCreateZombie: function() 
			{
				var tie;
				if (scope.paused)
					return;


				//console.log("checking if " + (gameBoardService.numBlocks + zombies.length).toString() + " >= " + (globalSettings.gameBoardWidth*globalSettings.gameBoardHeight));
				if (gameBoardService.numBlocks + zombies.length >= globalSettings.gameBoardWidth*globalSettings.gameBoardHeight)
					return;
			

				var bust = false;
				for (var i=0; i<rate; i++)
				{
					if (utilsService.random(globalSettings.zombieCreationChance) === 0) {

						while (true)
						{
							//console.log("checking if " + (gameBoardService.numBlocks + zombies.length).toString() + " >= " + (globalSettings.gameBoardWidth*globalSettings.gameBoardHeight));
							if (gameBoardService.numBlocks + zombies.length >= globalSettings.gameBoardWidth*globalSettings.gameBoardHeight)
								return;

							var x = utilsService.random(globalSettings.gameBoardWidth);
							var y = utilsService.random(globalSettings.gameBoardHeight);
							var bf = blockFree(x,y);
							var zf = zombieFree(x,y,-1);
							//console.log("zf: "+zf+" bf: "+bf);
							if (zf && bf && playerService.getPosition().x !== x && playerService.getPosition().y !== y)
							{
								create(x, y);
								spawned++;
								break;
							}
						}
					}
				}
            },

            damage: function(index) {
				zombies[index].health--;
				if (zombies[index].health <= 0)
					zombies.splice(index,1);
            },

            destroy: function(index) {
				zombies.splice(index,1);
            },

            destroyAll: function(index) {
				zombies = [];
            },

            checkCollision: function(x, y) {
				if (scope.paused)
					-1;

                for (var i=0; i<zombies.length; i++)
				{
					var zombie = zombies[i];
					if (zombie.x === x && zombie.y === y)
						return i;
				}
				return -1;
            },

            update: function () {

				if (scope.paused)
					return;

				for (var i=0; i<zombies.length; i++)
				{
					var zombie = zombies[i];
					if (!zombie)
						continue;

					var px = playerService.getPosition().x;
					var py = playerService.getPosition().y;

					zombie.sx += (sign(zombie.x-zombie.prevX) / zombie.frames) * zombie.speed;
					zombie.sy += (sign(zombie.y-zombie.prevY) / zombie.frames) * zombie.speed;

					zombie.clock++;
					if (zombie.clock % (zombie.frames/zombie.speed) !== 0)
						continue;
					
					zombie.prevX = zombie.x;
					zombie.prevY = zombie.y;
					zombie.sx = 0;
					zombie.sy = 0;

					var rand = utilsService.random(2);
					if (zombie.x === px)
						rand = 1;
					if (zombie.y === py)
						rand = 0;
					if (zombie.stuck === 'left' || zombie.stuck === 'right')
						rand = 1;
					if (zombie.stuck === 'top' || zombie.stuck === 'bottom')
						rand = 0;

					var override = utilsService.random(2)
					if (zombie.stuck === 'none')
						override = -1;

					var counter = 0;
					for (var j=0; j<1; j++)
					{
						counter++;
						if (counter > 2)
							break;
						//if (zombie.x !== zombie.prevX || zombie.y !== zombie.prevY || zombie.sx + zombie.prevX !== zombie.x || zombie.sy + zombie.prevY !== zombie.y)
						//	continue;

						if (rand === 0)
						{
							if (px < zombie.x || override === 0)
							{
								zombie.stuck = 'none';
								var mx = zombie.x - 1;
								var my = zombie.y;
								var bf = blockFree(mx,my);
								if (zombieFree(mx,my,i) && bf)
									zombie.x--;
								else
								{
									zombie.stuck = 'left';
									j--;
									rand = 1;
								}
							}
							else if (px > zombie.x || override === 1)
							{
								zombie.stuck = 'none';
								var mx = zombie.x + 1;
								var my = zombie.y;
								var bf = blockFree(mx,my);
								if (zombieFree(mx,my,i) && bf)
									zombie.x++;
								else
								{
									zombie.stuck = 'right';
									j--;
									rand = 1;
								}
							}
						}
						else
						{
							if (py < zombie.y || override === 0)
							{
								zombie.stuck = 'none';
								var mx = zombie.x;
								var my = zombie.y - 1;
								var bf = blockFree(mx,my);
								if (zombieFree(mx,my,i) && bf)
									zombie.y--;
								else
								{
									zombie.stuck = 'bottom';
									j--;
									rand = 0;
								}
							}
							else if (py > zombie.y || override === 1)
							{
								zombie.stuck = 'none';
								var mx = zombie.x;
								var my = zombie.y + 1;
								var bf = blockFree(mx,my);
								if (zombieFree(mx,my,i) && bf)
									zombie.y++;
								else
								{
									zombie.stuck = 'top';
									j--;
									rand = 0;
								}
							}
						}

					}
				}
            },

            draw: function () {

				for (var i=0; i<zombies.length; i++)
				{
					var zombie = zombies[i];

					if (zombie.clock < 30)
						graphicsEngineService.drawImageModular(coordinateSystem.world, zombie.initX, zombie.initY, 'crack');
				}

				for (var i=0; i<zombies.length; i++)
				{
					var zombie = zombies[i];

					graphicsEngineService.drawImageModular(coordinateSystem.world, zombie.prevX + zombie.sx, zombie.prevY + zombie.sy, 'zombie');
					//graphicsEngineService.drawImageModular(coordinateSystem.world, zombie.prevX, zombie.prevY, 'player');
					//graphicsEngineService.drawImageModular(coordinateSystem.world, zombie.x, zombie.y, 'bullet');
				}
            }
        }

    }]);
