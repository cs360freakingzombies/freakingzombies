angular.module("gameApp")
.factory("saveService", ["globalSettings", "bulletService", "gameBoardService", "gameService", "gameStateService", "graphicsEngineService","playerService", "renderService","zombieService", function(globalSettings, bulletService, gameBoardService, gameService, gameStateService, graphicsEngineService, playerService, renderService, zombieService) {
	
	return {

			getSaveModel : function() {
				var myJSON = 
				{
					"bullets" : bulletService.getBullets(),
					"map" : gameBoardService.map,
					"gameState" : gameStateService,
					"time" : gameStateService.getMilleseconds(),
					"player" : playerService.getPlayer(),
					"zombies" : zombieService.getZombies()
				};
				console.log("myJSON: " + myJSON);
				return myJSON;
			}
	}
	
 }]);
