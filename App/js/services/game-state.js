angular.module("gameApp")
    
    .factory("gameStateService", ["globalSettings", "gameState", function(globalSettings, gameState) {

	var scope = {};

	function pad(n, width, z)
	{
		z = z || '0';
		n = n + '';
		return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	}

	return {
	    health: globalSettings.health,
	    immunityTime: 0,
		gameState: gameState.gameActive,
		playerDieTime: null,
	    firingDirection: 87,
	    startTime: new Date(),
		offset: 0, // the load function should add the number of milliseconds that the loaded game had to this number and set the startTime to new Date()
		pausedTime: null,
		pausedTimeFormatted: null,
		getMilleseconds : function()
		{
	    	var presentTime = new Date();
			return presentTime.getTime() - this.startTime.getTime();
		},
	    time: function()
		{
	    	var presentTime = new Date();
			var diff = presentTime.getTime() - this.startTime.getTime() + this.offset;
			var seconds = Math.floor(diff/1000)%60;
			var minutes = Math.floor(diff/(60*1000))%60;
			var hours = Math.floor(diff/(60*60*1000));
			var ret = "";
			ret = pad(hours,2) + ':' + pad(minutes,2) + ':' + pad(seconds,2);

			if (scope.paused)
			{
				if (this.pausedTime === null)
				{
					this.pausedTime = new Date();
					this.pausedTimeFormatted = ret;
					this.offset += presentTime.getTime() - this.startTime.getTime();
				}
				return this.pausedTimeFormatted;
			}
			else
			{
				if (this.pausedTime !== null)
				{
					this.startTime = new Date();
					this.pausedTime = null;
					return this.pausedTimeFormatted;
				}
			}
			
			return ret;
		},
	    //currentTime: this.calcTime(),
	    initialise: function(scp) {
				this.offset = 0;
				this.startTime = new Date();
				scope = scp;
			},

            hasGameOverTransitionComplete: function() {
                var nowTime = new Date();

                return nowTime - this.playerDieTime > globalSettings.delayAfterDeathBeforeNewGameStart;
            },

            hasPlayerDeathTransitionComplete: function() {
                var nowTime = new Date();

                return nowTime - this.playerDieTime > globalSettings.delayAfterDeathBeforePlayerRegeneration;
            },

            isGameOver: function() {
                return this.gameState === gameState.gameOver;
            },
    
            reset: function() {
				this.health = globalSettings.health;
                this.gameState = gameState.gameActive;
				this.offset = 0;
				this.startTime = new Date();
            },
    
            die: function() {
				if (scope.paused)
					return false;
				if(this.immunityTime > 0)
					return false;
				if(this.health > 50)
				{
						this.health -= 50;
						this.immunityTime = globalSettings.immunityTime;
				}
				else
				{
					this.health = 0;
                    this.gameState = gameState.gameOver;
					return true;
                }
				return false;
            },

            playerRegenerate: function() {
                this.gameState = gameState.gameActive;
            }
        }
    }]);
