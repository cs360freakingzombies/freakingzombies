angular.module("gameApp")
    .factory("renderService", ["graphicsEngineService", "gameBoardService", "globalSettings", "gameStateService", "sprite", "scoreMarkerService", "zombieService", "bulletService", "playerService", "coordinateSystem",
        function(graphicsEngineService, gameBoardService, globalSettings, gameStateService, sprite, scoreMarkerService, zombieService, bulletService, playerService, coordinateSystem) {
            "use strict";

            var livesAnimationCount = 0;

            function blankScreen() {
                graphicsEngineService.blankScreen();
            }

            function drawBoard() {
                gameBoardService.draw();
            }

			function drawLine() {
				graphicsEngineService.drawLine(0,40,600,40);
			}

            function drawGameStateService() {
                if (gameStateService.isGameOver()) {
                    graphicsEngineService.drawText(
                        coordinateSystem.screen,
                        globalSettings.gameOverXPosition,
                        globalSettings.gameOverYPosition,
                        "Game Over",
                        globalSettings.gameOverFontColour,
                        globalSettings.gameOverFont);
                }
            }

            function drawScoreBoard(animation) {
                if (animation == 0) {
                    livesAnimationCount++;

                    if (livesAnimationCount == 9) {
                        livesAnimationCount = 0;
                    }
                }

                var animationOffset = livesAnimationCount;
                if (livesAnimationCount > 4) {
                    animationOffset = 9 - livesAnimationCount;
                }

                graphicsEngineService.drawText(
                    coordinateSystem.screen,
                    globalSettings.scoreBoardLivesXPositionText,
                    globalSettings.scoreBoardTitleYPosition,
                    "Health",
                    globalSettings.scoreBoardTitleFontColour,
                    globalSettings.scoreBoardFont);

				graphicsEngineService.drawText(
					coordinateSystem.screen,
					globalSettings.scoreBoardLivesXPositionText,
					globalSettings.scoreBoardContentYPosition,
					gameStateService.health,
					globalSettings.scoreBoardContentFontColour,
					globalSettings.scoreBoardFont);
                /*for (var i = 0; i < gameStateService.lives; i++) {
                    /*graphicsEngineService.drawImage(
                        coordinateSystem.screen,
                        globalSettings.scoreBoardLivesXPositionImage + (globalSettings.scoreBoardLivesOffset * i) + (animationOffset * 4),
                        globalSettings.scoreBoardLivesYPosition,
                        sprite.playerWalkRight1 + livesAnimationCount);*/
                //}
            }
			function drawGameTime(){  //coordSystem, x, y, text, colour, font
					graphicsEngineService.drawText(
						coordinateSystem.screen,
						globalSettings.elapsedTimeTitleXPosition,
						globalSettings.elapsedTimeTitleYPosition,
						globalSettings.elapsedTimeTitle,
						globalSettings.elapsedTimeTitleFontColour,
						globalSettings.elapsedTimeTitleFont);
						
						
					graphicsEngineService.drawText(
						coordinateSystem.screen,
						globalSettings.elapsedTimeContentXPosition,
						globalSettings.elapsedTimeContentYPosition,
						gameStateService.time(),
						globalSettings.elapsedTimeContentFontColour,
						globalSettings.elapsedTimeContentFont);
			}

            return {
                draw: function(animation) {
                    blankScreen();
                    drawScoreBoard(animation);
					drawGameTime();
                    drawBoard();
					drawLine();
                    playerService.draw(animation);
                    zombieService.draw();
                    bulletService.draw(animation);
                    scoreMarkerService.draw(animation);
                    drawGameStateService();
                }
            }
        }]);
