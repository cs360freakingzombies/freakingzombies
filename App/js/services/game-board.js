angular.module("gameApp")
    .factory("gameBoardService", ["globalSettings", "utilsService", "graphicsEngineService", "gameStateService", "boardLocation", "characterDirection", "sprite", "coordinateSystem", function(globalSettings, utilsService, graphicsEngineService, gameStateService, boardLocation, characterDirection, sprite, coordinateSystem) {
        "use strict";

        function inPlayerArea(row, height) {
            return row > height - globalSettings.playerAreaHeight;
        }

        return {
            initialise: function () {
                this.map = [];
                this.numBlocks = 0;
    
                this.generateBlocks();
            },
    
            generateBlocks: function () {
                for (var h = 0; h < globalSettings.gameBoardHeight; h++) {
                    this.map.push([]);
                    for (var w = 0; w < globalSettings.gameBoardWidth; w++) {
                        this.map[h].push(0);
    
                        // no blocks on the bottom row
                        if (h > 0 && w > 0) {
                            if (utilsService.random(globalSettings.blockChance) === 0) {
								if (this.map[h][w] === 0)
									this.createBlock(w, h);
								if (this.map[h][w-1] === 0)
									this.createBlock(w-1, h);
								if (this.map[h-1][w] === 0)
									this.createBlock(w, h-1);
								if (this.map[h-1][w-1] === 0)
									this.createBlock(w-1, h-1);
                            }
                        }
                    }
                }
            },
    
            createBlock: function (x, y) {
				this.numBlocks++;
    
                this.map[y][x] = 4;
            },
    
            poisonBlock: function (x, y) {
                if (this.map[y][x] > 0) {
                    this.map[y][x] *= -1;
                }
            },
    
            destroyBlock: function (x, y) {
                if (this.checkCollision(x, y) !== boardLocation.space) {
                    this.numBlocks--;
                    this.map[y][x] = boardLocation.space;
                }
            },
    
            playerAllowedToMove: function (currentX, currentY, direction) {
                var x = currentX;
                var y = currentY;
    
                switch (direction) {
                    case characterDirection.down:
                        y += 1;
                        break;
                    case characterDirection.up:
                        y -= 1;
                        break;
                    case characterDirection.right:
                        x += 1;
                        break;
                    case characterDirection.left:
                        x -= 1;
                        break;
                }
    
                if (x >= globalSettings.gameBoardWidth || x < 0 || y >= globalSettings.gameBoardHeight || y < 0 /*|| !inPlayerArea(y, globalSettings.gameBoardHeight)*/) {
                    return false;
                }
				
				/*if(x >= globalSettings.gameBoardWidth)
					x = 0;
				else if(x < 0)
					x = globalSettings.gameBoardWidth - 1;
				else if(y >= globalSettings.gameBoardHeight)
					y = 0;
				else if(y < 0)
					y = globalSettings.gameBoardHeight - 1;*/
    
                return this.map[y][x] === 0;
            },
    
            checkCollision: function (x, y, destroyLocation) {
				if (x < 0 || y < 0 || x > globalSettings.gameBoardWidth-1 || y > globalSettings.gameBoardHeight-1)
					return true;
    			return this.map[y][x] > 0;
            },
    
            draw: function () {
                var incrementLevelTransitionCount = false;
    
                for (var h = 0; h < globalSettings.gameBoardHeight; h++) {
                    for (var w = 0; w < globalSettings.gameBoardWidth; w++) {
                        var blockStrength = this.map[h][w];
                        if (blockStrength) {
							graphicsEngineService.drawImageModular(coordinateSystem.world, w, h, 'block');
                        }
                    }
                }
    
                if (incrementLevelTransitionCount) {
                    gameStateService.incrementLevelTransitionLineCount();
                }
            }
        };
    }]);
