angular.module("gameApp", [])

    .controller("appController", ["$scope", "$interval", "$http", "keyPressHandlerService", "mouseService", function($scope, $interval, $http, keyPressHandlerService, mouseService) {
		$scope.onMouseMoveResult = "";
		$scope.onMouseDownResult = "";
		
	$scope.plays = 0;

	$scope.update = function() {
		$http.get('/plays').
		success(function(data, status, headers, config)
		{
			$scope.plays = data.count;
		}).
		error(function(data, status, headers, config)
		{
			console.log("error: "+data);
		});
	}
	$interval($scope.update, 1000);
	$scope.update();
	
	$scope.play = function() {
		$scope.instructionsDisplayed = true;
	}

	$scope.keydown = function(keyEvent) {
		keyPressHandlerService.keyPress(keyEvent.keyCode);
	};

	$scope.keyup = function(keyEvent) {
		keyPressHandlerService.keyRelease(keyEvent.keyCode);
	};
	$scope.onMouseMove = function(mouseEvent){
		$scope.onMouseMoveResult = mouseEvent.clientX + ':' + mouseEvent.clientY;
		//mouseService.mouseMove(mouseEvent);
	}
	$scope.onMouseDown = function(mouseEvent){
		$scope.onMouseDownResult = 'last click' + mouseEvent.clientX + ':' + mouseEvent.clientY;
	}
    }])

    .directive("freakingZombiesGame", ["$interval", "gameService", "zombieService", "playerService", "gameStateService", "renderService", "graphicsEngineService", "saveService", "$http", "gameBoardService", "bulletService",  function($interval, gameService, zombieService, playerService, gameStateService, renderService, graphicsEngineService, saveService, $http, gameBoardService, bulletService) {
        return {
            restrict: 'A',
            template: '<div id="content"><div id="rightside"><button class="functional" ng-click="save()">Save</button><button class="functional" ng-click="pause()">{{pausetext}}</button><br><button class="functional" ng-click="load()">Load: </button><input id="loadinput" placeholder="id" type="text" ng-model="loadid" name="lastname"><br><span id="logdisplay"><pre>{{logcontent}}</pre></span></div><div id="gameholder"><canvas id="gameCanvas" width="600" height="640" style="border:1px solid #000000;"></canvas></div></content>',

            link: function(scope, element) {
                var intervalPromise;
                var animation = 0;
                var canvas = element.find('canvas')[0].getContext("2d");

				var graphicsDir = 'App/img/';
				var graphicsArray = ['bullet.png', 'explosion.png', 'zombie.png', 'player.png', 'gun.png', 'block.png', 'crack.png'];
				graphicsEngineService.initialise(canvas, graphicsArray, graphicsDir);
				gameService.initialise(scope);
				zombieService.initialise(scope);
				playerService.initialise(scope);
				gameStateService.initialise(scope);

				scope.pausetext = "Pause";
				scope.paused = false;
				scope.loadid = "";

				var time = new Date().toTimeString().split(' ')[0];
				scope.logcontent = time + ": initialized\n\n";

				scope.pause = function() {
					time = new Date().toTimeString().split(' ')[0];
					var action = "";
					if (scope.paused)
					{
						action = "resumed";
						scope.paused = false;
						scope.pausetext = "Pause";
					}
					else
					{
						action = "paused";
						scope.paused = true;
						scope.pausetext = "Resume";
					}
					scope.logcontent = time + ": " + action + "\n\n" + scope.logcontent;
				}

				scope.save= function() {
					time = new Date().toTimeString().split(' ')[0];
					var sm = saveService.getSaveModel();
					console.log("sm: " + sm);

					$http.post('/save', sm).
					success(function(data, status, headers, config)
					{
						console.log("success saving: "+JSON.stringify(data));
						result = data.id;
						scope.loadid = result;
						scope.logcontent = time + ": saved with id:  " + scope.loadid + "\n\n" + scope.logcontent;
					}).
					error(function(data, status, headers, config)
					{
						// called asynchronously if an error occurs
						// or server returns response with an error status.
						console.log("error: "+data);
						scope.logcontent = time + ": error saving\n\n" + scope.logcontent;
					});
				}

				scope.load = function() {
					time = new Date().toTimeString().split(' ')[0];

					var isHex = scope.loadid.match("[0-9a-f]{24,24}");
					if (scope.loadid === '' || !isHex)
					{
						scope.logcontent = time + ": please provide a game id to load\n\n" + scope.logcontent;
						return;
					}

					$http.post('/load', {"id":scope.loadid}).
					success(function(data, status, headers, config)
					{
						console.log("success loading: "+JSON.stringify(data));
						console.log("success zombies: "+JSON.stringify(data.data.zombies));
						console.log("success gameState: "+JSON.stringify(data.data.gameState));

						// actually update the game
						zombieService.setZombies(data.data.zombies);
						playerService.setPlayer(data.data.player);
						var gs = data.data.gameState;
						gameStateService.health = gs.health;
						gameStateService.gameState = gs.gameState;

						gameStateService.offset = data.data.time;
						gameStateService.startTime = new Date();
						gameStateService.pausedTime = null;

						gameStateService.playerDieTime = gs.playerDieTime;
						gameBoardService.map= data.data.map;
						bulletService.setBullets(data.data.bullets);

						scope.logcontent = time + ": loaded with id: " + scope.loadid + "\n\n" + scope.logcontent;
					}).
					error(function(data, status, headers, config)
					{
						// called asynchronously if an error occurs
						// or server returns response with an error status.
						console.log("error: "+data);
						scope.logcontent = time + ": error loading\n\n" + scope.logcontent;
					});

				}

                function gameLoop() {
					if (!scope.paused)
						animation++;

                    if (animation == 4) {
                        animation = 0;
                    }

					//console.log("ani: " + animation);
                    gameService.update(animation);
                    renderService.draw(animation);
                }

                intervalPromise = $interval(gameLoop, 50);

                scope.$on("$destroy", function() {
                    if (intervalPromise) {
                        $interval.cancel(intervalPromise);
                        intervalPromise = undefined;
                    }
                });
            }
        }
    }]);
